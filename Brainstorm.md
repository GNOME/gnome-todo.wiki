Random ideas for GNOME To Do. There are absolutely no guarantees that any idea
listed here will be implemented.

### Data storage

Evolution-Data-Server has served us well, but it feels dated and limiting. Seems
a new backend made specially for GNOME To Do will be a better direction.

Remove integration with external services (Google Tasks, NextCloud tasks, etc)
in favour of importing tasks from these services, and online backups.

### Local network synchronization

Sharing data between devices in the local network sounds like a good idea.
