Task management is extremely personal, and no method works for everyone. The
same individual can even change their task management habits over time.

As a personal task management application, GNOME To Do has a default workflow,
but allows some degree of fine tuning.

# Task lifecycle

Tasks follow a lifecycle that consists of 3 phases:

 * Capture
 * Processing
 * Execution

Each phase changes the state of the task, starting from the uncaptured state,
leading up to the task completion. The goal of GNOME To Do is cover all 3
phases, and offer tools for people to process and execute tasks easily.

## Phase 1: Capture

Capturing a task is the act of storing the task in a permanent storage - your
hard drive. Effective task capturing must be seamless and effortless.

Traditionally, capturing is implemented as an entry that you input your task
name and save it. I believe this is an innefficient way to capture tasks,
because it requires you to switch to that application or browser tab, go to
the entry, add the task, and switch back.

Ideally, capturing a task shouldn't require switching apps, and should basically
be instantaneous.

One way to implement this ideal capturing is through a GNOME Shell extension
that pops up a system dialog when a certain keyboard shortcut is pressed. ¹

Captured tasks go to the inbox, which is the list of unprocessed tasks.


## Phase 2: Processing

Processing a task consists of moving it to an appropriate task list, or doing it
right away if it's a trivial task, or even throwing it in the wastebasket if you
don't plan on doing it.

Optionally, adding more description, setting an end date, and marking it as
important are done in this step.

There's a variety of methods to process and organize tasks:

 * Eisenhower matrix (urgent × important)
 * Kanban board
 * Tagging
 * Gantt
 * Dynamic lists
 * …

Ideally, GNOME To Do would offer many of these methods.


## Phase 3: Executing

Executing a task is what leads the task to its conclusion.

GNOME To Do cannot control the task execution itself, but it can help easing
execution in a variety of ways:

 * Timers and time management tools 
 * Reminders
 * Automatic startup
 * Neutral encouragement
 * …

Again, ideally, GNOME To Do would offer many of these methods.


# High level planning

TODO


---

¹ - A more ellegant solution would be negotiating a gloabl hotkey with the
compositor that, when triggered, would open GNOME To Do in a minified mode with
only the task capturing entry, and the window would hide after adding the task.
However, this is not yet possible on Wayland compositors.
